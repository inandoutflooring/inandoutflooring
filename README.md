We are a local family business, committed to giving our customers an optimum experience when buying materials or installing floors in their spaces!

We have been in the flooring business for the past 6 years servicing the greater Birmingham area.

Our services include the removal and installation of all types of flooring including, but not limited to Hardwood, Ceramic Tile, Luxury Vinyl Plank, Engineered Wood, Laminate, Carpet and more!

In and out Flooring, flooring done right the first time. || 

Address: 120 19th St N, Unit #2041, Birmingham, AL 35203, USA ||
Phone: 205-530-6143 || 
Website: https://inandoutflooring.com/
